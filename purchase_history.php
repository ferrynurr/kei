
<?php include 'templates/header.php';?>
<?php include 'templates/topbar.php';?>

<div class="content-home" style="height: 100vh;">
    <div class="container" style="margin-top:2rem">
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-default box-purchase">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-6">
                                <div>
                                    <p><strong>No.Order : SPX54RE5454545</strong></p>
                                    <p><strong>Tanggal : <?= date('d-m-Y') ?></strong></p>
                                </div>
                                <div style="margin-top:2rem;">
                                    <span class="label label-danger">Menunggu pembayaran</span>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6 text-right">
                                <i class="fa fa-exclamation-circle fa-2x"></i>
                                <div style="margin-top: 2rem;">
                                    <p><small>Total item: 7</small></p>
                                    <h4><strong style="color: #157ed2;">Rp 200.000</strong></h4>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-default box-purchase">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6 col-xs-6">
                                <div>
                                    <p><strong>No.Order : SPX54RE5454545</strong></p>
                                    <p><strong>Tanggal : <?= date('d-m-Y') ?></strong></p>
                                </div>
                                <div style="margin-top:2rem;">
                                    <span class="label label-warning">Menunggu dikirim</span>
                                </div>
                            </div>
                            <div class="col-md-6 col-xs-6 text-right">
                                <i class="fa fa-exclamation-circle fa-2x"></i>
                                <div style="margin-top: 2rem;">
                                <small>Total item: 7</small>
                                    <h4><strong style="color: #157ed2;">Rp 200.000</strong></h4>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
           
        </div>
    </div>

</div>



<div id="modalDetailPurchase" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog " role="document">
        <div class="modal-content" style="padding: 1rem;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="noOrderPurchase"></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6 text-left">
                        <strong>Tanggal: 12-09-2023</strong>
                    </div>
                    <div class="col-md-6 text-right">
                        <span class="label label-danger">Menunggu pembayaran</span>
                    </div>
                </div>

                <div style="margin-top:4rem;">
                    <table class="table table-hover table-bordered" id="tblPurchaseDetail">
                        <thead style="background-color: #d0e7f8;">
                            <tr>
                                <th>No. Item</th>
                                <th>Nama</th>
                                <th class="text-center">Harga</th>
                                <th class="text-center">Qty</th>
                                <th class="text-center">Subtotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>Mesin</td>
                                <td class="text-center">30.0000</td>
                                <td class="text-center">3</td>
                                <td class="text-center">90.0000</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Mesin 2</td>
                                <td class="text-center">20.0000</td>
                                <td class="text-center">2</td>
                                <td class="text-center">40.0000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="row" style="margin-top:1rem;">
                    <div class="col-md-6">
                        <div class="panel panel-default btn-rounded">
                            <div class="panel-body text-left">
                                <strong>Informasi Rekening :</strong>
                                <p>
                                    <small>123.456.789</small><br>
                                    <small>a/n PT.Karya Energi Indonesia </small>
                                </p>
                                <p><small>Bank BCA</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row" >
                            <div class="col-md-6 text-left"><strong><small>Total Nilai</small></strong></div>
                            <div class="col-md-6 text-right"><strong><small>Rp 50.000.000</small></strong></div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 text-left"><small>Pajak</small></div>
                            <div class="col-md-6 text-right"><small>Rp 200.000</small></div>
                        </div>
                        <div class="row" >
                            <div class="col-md-6 text-left"><small>Ongkos Kirim</small></div>
                            <div class="col-md-6 text-right"><small>Rp 20.000</small></div>
                        </div>
                        <div class="row">
                            <hr style="margin:10px; border-top:1px solid #e0e0e0;" >
                            <div class="col-md-6 text-left"><strong>Grand Total</strong></div>
                            <div class="col-md-6 text-right"><strong>Rp 50.220.000</strong></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default btn-sm btn-rounded"> <i class="fa fa-close"></i> Tutup</button>
            </div>
        </div>
    </div>
</div>

<?php include 'templates/footer.php';?>

<script>
    $(document).ready(function () {

        $('body').on('click', '.box-purchase', function(e){
                onClickDetailPurcahse(e);
            });
    });

    function onClickDetailPurcahse(e) {
        $('#noOrderPurchase').text('SO/23/0003/00002');
        $('#modalDetailPurchase').modal('show');
    }
</script>