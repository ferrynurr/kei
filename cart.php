
<?php include 'templates/header.php';?>
<?php include 'templates/topbar.php';?>

<style>
    .btnScrollToTop {
        bottom: 65px !important;
    }
</style>

<div class="content-home">
    <div class="container" style="margin-top:2rem; padding-bottom: 10rem;">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="panel panel-default btn-rounded panel-shadow">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover" id="tblCart">
                                <thead>
                                    <tr>
                                        <th>
                                            <div class="pretty p-icon p-round p-smooth">
                                                <input name="chbListCartAll" type="checkbox" checked/>
                                                <div class="state p-primary">
                                                    <i class="icon fa fa-check"></i> 
                                                    <label></label>
                                                </div>
                                            </div>
                                        </th>
                                        <th>Product</th>
                                        <th>Qty</th>
                                        <th class="text-center">Price (Rp)</th>
                                        <th class="text-center">Total (Rp)</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- <div class="container" style="display: none;">
        <p style="margin-bottom: 3rem;"><strong style="font-size: 23px;">Keranjang saya</strong></p> 

        <div class="pretty p-icon p-round p-smooth" style="font-size: 18px;">
            <input type="checkbox" />
            <div class="state p-primary">
                <i class="icon fa fa-check"></i>
                <label> Pilih Semua</label>
            </div>
        </div>
        <div class="row" style="padding-top:1rem;">
            <div class="col-md-8">
                <div id="listCartProduct"></div>


            </div>
            <div class="col-md-4">
                <div class="panel panel-default btn-rounded panelInfoHarga">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h4><strong>Ringkasan Belanja</strong></h4>
                                <p>Total Harga (10 Barang) </p>
                                <p>Pajak </p>
                                <p>Ongkos Kirim </p>
                            </div>
                            <div class="col-md-6 text-right">
                                <h4>&nbsp;</h4>
                                <h4><strong> Rp 3.000.0000</strong></h4>
                                <p><strong> Rp 30.0000</strong></p>
                                <p><strong> Rp 20.0000</strong></p>
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="margin-bottom: 2rem;">
                            <div class="col-md-6"><h4><strong>Total Harga</strong></h4></div>
                            <div class="col-md-6 text-right"><h4><strong>Rp 3.050.0000</strong></h4></div>
                        </div>
                        <button class="btn btn-success btn-rounded" style="width: 100%;" type="button">Beli</button>
                    </div>
                </div>
            </div>
        </div>
        
    </div> -->
</div>
<div class="footer-cart">
    <div class="footer-cart-body">
        <div class="row" style="padding-left: 5rem;padding-right:5rem; padding-top: 1rem;">
            <div class="col-md-8 col-xs-6 text-left">
                <p><small id="totSelected">Total Harga</small><br>
                <strong id="totPrice">Rp 3.077.000</strong></p>
            </div>
            <div class="col-md-4 col-xs-6 text-right">
                <button id="btnBeli" class="btn btn-primary btn-rounded panel-shadow" style="width: 100%;" type="button">Beli</button>
            </div>
        </div>
        
    </div>
</div>
<?php include 'templates/footer.php';?>

<script>
    $(document).ready(function () {
        getListProduct();
        
        $('body').on('click', '#btnBeli', function(e){
            onClickBuy(e);
        });

        $('body').on('change', '[name="chbListCartAll"]', function(e){
        // $('[name="chbListCartAll"]').change(function(e) {
            onChangeSelAll(e, this);
        });

        $('body').on('change', '.chbListCart', function(e){
        // $('[name="chbListCartAll"]').change(function(e) {
            onChangeSel(e, this);
        });

        setTimeout(() => {
            let totCheck = $('[name="chbListCart[]"]:checked').length;
            $('#totSelected').text(`Total Harga (${totCheck} Dipilih)`);
        }, 800);

       
    });

    function getListProduct(){

        $.ajax({
            type: 'GET',
            url: 'data-cart-item.json',
            dataType: "json",
            cache: false,
            success: function(response){
                response = response.sort((a, b) => {
                    if (a.id < b.id) {
                        return -1;
                    }
                });
                createListCard(response);
                console.log(response)
            },
            error: function(xhr, testStat){
                console.error(xhr.responseText);
                $('#listCartProduct').html('');
            }
        });

    }

    function createListCard(dataList) {
        if(dataList.length > 0){
            $.each(dataList, function (i, row) {
                $.ajax({
                    type: 'GET',
                    url: 'data-products.json',
                    dataType: "json",
                    cache: false,
                    success: function(response){
                        let produk = response.filter(function (val) {
                            return val.id == row.id;
                        });

                        console.log(produk);
                        var titleSplit = produk[0].title.length > 40 ? 
                                            produk[0].title.substring(0, 40 - 3) + "..." : 
                                            produk[0].title;
                                            
                        var descSplit = produk[0].desc.length > 50 ? 
                                            produk[0].desc.substring(0, 50 - 3) + "..." : 
                                            produk[0].desc;


                        $('#tblCart').find('tbody').append(buildRow(titleSplit, descSplit, produk[0].photo, row.qty, produk[0].price_disc));

                        // $('#listCartProduct').html($('#listCartProduct').html()+html);
                      
                    },
                    error: function(xhr, testStat){
                        console.error(xhr.responseText);
                    }
                });
              
            });
        }
        
        // return html;
    }

    function loading() {
        let html =`
            <div class="text-left" style="margin-top:5vh;">
                <i class="fa fa-spinner fa-spin fa-2x"></i>&nbsp;&nbsp;
                <span style="font-size:15px"> Sedang memuat data...</span>
            </div>
        `;
        return html;

    }

    function buildRow(title, desc, photo, qty, price){
        let total = parseInt(price) * parseInt(qty);
        return `
            <tr>
                <td class="col-md-1 col-xs-1" >
                    <div class="pretty p-icon p-round p-smooth">
                        <input name="chbListCart[]" class="chbListCart" type="checkbox" checked />
                        <div class="state p-primary">
                            <i class="icon fa fa-check"></i> 
                            <label></label>
                        </div>
                    </div>
                </td>
                <td class="col-md-7 col-xs-5">
                    <div class="media">
                        <a class="thumbnail pull-left" href="#"> 
                        <img class="media-object" src="${photo}" style="width: 72px; height: 72px;"> </a>
                        <div class="media-body" style="padding-left:1rem;">
                            <h4 class="media-heading"><a href="#">${title}</a></h4>
                            <h5 class="media-heading"> by <a href="#">KEI</a></h5>
                            <span>${desc}</span>
                        </div>
                    </div>
                </td>
                <td class="col-md-1 col-xs-2" style="text-align: center">
                    <input type="text" class="form-control" id="edtQty" value="${qty}">
                </td>
                <td class="col-md-1 col-xs-2 text-center"><strong>${price}</strong></td>
                <td class="col-md-2 col-xs-2 text-center"><strong>${total}</strong></td>
                <td class="col-md-1 col-xs-1">
                    <button type="button" class="btn btn-danger btn-sm btn-rounded">
                        <i class="fa fa-trash btn-rounded"></i>
                    </button>
                </td>
            </tr>
        `;
    }

    function onClickBuy(e){
        Swal.fire(
        'Success!',
        'Terima kasih telah berbelanja ditempat kami, pesanan Anda akan segera diproses setelah ada konfirmasi pembayaran',
        'success'
        );

        setTimeout(() => {
            window.location.href= "./";
        }, 1800);
    }

    function onChangeSelAll(e, self){
        if ($(self).is(':checked')) {
            $('.chbListCart').prop('checked', true);
            $('#totPrice').text('Rp 3.077.000');
        }else{
            $('.chbListCart').prop('checked', false);
            $('#totPrice').text('Rp 0');
        }

        let totCheck = $('[name="chbListCart[]"]:checked').length;
        if(totCheck > 0)
            $('#btnBeli').attr('disabled', false);
        else 
            $('#btnBeli').attr('disabled', true);

        $('#totSelected').text(`Total Harga (${totCheck} Dipilih)`);
    }

    function onChangeSel(e, self){

            let totCheck = $('[name="chbListCart[]"]:checked').length;
            if(totCheck > 0 ){
                $('#btnBeli').attr('disabled', false);
                $('#totPrice').text('Rp 3.077.000');
                $('[name="chbListCartAll"]').prop('checked', true);
            }else {
                $('#btnBeli').attr('disabled', true);
                $('#totPrice').text('Rp 0');
                $('[name="chbListCartAll"]').prop('checked', false);
            }

            $('#totSelected').text(`Total Harga (${totCheck} Dipilih)`);
    }
</script>
