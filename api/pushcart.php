<?php 
    $newDataId = $_GET['id'];
    $newQty = $_GET['qty'];

    $json = file_get_contents('../data-cart-item.json');

    if($json == ""){
        file_put_contents("../data-cart-item.json", json_encode([]));
        $json = file_get_contents('../data-cart-item.json');
    }

    $oldData =  json_decode($json, true);
    $newData = [];
  
    if(count($oldData) > 0){
        echo json_encode($oldData);
        $temp=[];
        $st=false;
        foreach ($oldData as $key => $value) {
            $arr =[];
            if( $value['id'] == $newDataId){
                $arr['id'] = $value['id'];
                $arr['qty'] = (int) $value['qty'] + (int) $newQty;
                
            }else{
                $arr['id'] = $value['id'];
                $arr['qty'] = $value['qty'];
            }
            $temp[] = $arr;
        }

        if(count($temp) > 0){
            $st=false;
            foreach ($temp as $key => $value) {
                if($value['id'] == $newDataId){
                    $st = true;
                }
            }

            $tmp_Add =[];
            if(!$st){
                $tmp_Add[] = [
                    'id' => $newDataId,
                    'qty' => $newQty 
                ];
            }

            $newData = array_merge($temp,  $tmp_Add);
        }else{
            $newData = $oldData;
        }

    }else{
        $newData[] = [
                'id' => $newDataId,
                'qty' =>  $newQty 
            ];

        echo json_encode($newData);
    }

   
    $writeJson = file_put_contents("../data-cart-item.json", json_encode($newData)); //write the json array to

?>