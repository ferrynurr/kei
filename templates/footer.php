
<div id="btnScrollTop" class="btnScrollToTop btn-rounded">
    <i class="fa fa-angle-up fa-2x"></i>
</div>
   
   <div id="modalLogin" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content" style="padding: 1rem;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Masuk</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control btn-rounded" value="" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control btn-rounded" value="" placeholder="Password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button id="btnProsesLogin" type="button" class="btn btn-primary btn-rounded" style="width: 100%;">
                           <i class="fa fa-sign-in"></i> Masuk
                    </button>
                    <div class="row" style="padding-top: 1rem;">
                        <div class="col-md-6 text-left">
                            <a href="#" style="font-size: 12px;">Lupa password?</a>
                        </div>
                        <div class="col-md-6  text-right">
                            <a href="#" style="font-size: 12px;">Belum punya akun?</a>
                        </div>
                    </div>
                    <div class="text-center" style="margin-top:4rem;">
                        <strong style="font-size: 8px;">Versi 1.0</strong><br>
                        <strong style="font-size: 10px; color:#157ed2">KEI Warehouse</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modalProfile" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog " role="document">
            <div class="modal-content" style="padding: 1rem; background-color:#e5e5e5">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Profil User</h4>
                </div>
                <div class="modal-body">
                    <div class="row" style="padding-top:2rem;">
                        <div class="col-md-10 col-xs-12">
                            <div class="row">
                                <div class="col-md-3 col-xs-4 text-center" style="padding-top: 1rem;">
                                    <i class="fa fa-user-circle fa-5x"></i>
                                </div>
                                <div class="col-md-9 col-xs-8 text-left" style="padding: 1rem;">
                                    <strong style="font-size: 19px;">M. Ferry Nurdin</strong><br>
                                    <small style="font-size: 12px;">085736345604</small><br>
                                    <small style="font-size: 12px;">ferry@myacts.com</small>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2 col-xs-12 text-center" style="padding: 1rem;">
                          <button type="button" style="width: 100%;" class="btn btn-info btn-xs btn-rounded panel-shadow"><i class="fa fa-pencil"></i> Edit</button>
                        </div>
                    </div>

                    <div style="margin-top:3rem; padding:2rem;">
                        <div id="listPurchaseHistory" class="row btn-rounded list-profile" >
                            <div class="col-xs-8 col-md-10" style="margin-top:4px;">
                                <i class="fa fa-history"></i> <strong> Purchase History</strong>
                            </div>
                            <div class="col-xs-4 col-md-2 text-right">
                                <i class="fa fa-angle-right fa-2x"></i>
                            </div>
                        </div>
                        <div class="row btn-rounded list-profile" >
                            <div class="col-xs-8 col-md-10" style="margin-top:4px;">
                                <i class="fa fa-home"></i> <strong>Daftar Alamat</strong>
                            </div>
                            <div class="col-xs-4 col-md-2 text-right">
                                <i class="fa fa-angle-right fa-2x"></i>
                            </div>
                        </div>
                        <div class="row btn-rounded list-profile" >
                            <div class="col-xs-8 col-md-10" style="margin-top:4px;">
                            <i class="fa fa-lock"></i> <strong>Ubah kata sandi</strong>
                            </div>
                            <div class="col-xs-4 col-md-2 text-right">
                                <i class="fa fa-angle-right fa-2x"></i>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button name="btnKeluar" type="button" class="btn btn-danger btn-rounded panel-shadow" style="width: 100%;">
                           <i class="fa fa-sign-in"></i> Keluar
                    </button>
                  
                    <div class="text-center" style="margin-top:4rem;">
                        <strong style="font-size: 8px;">Versi 1.0</strong><br>
                        <strong style="font-size: 10px; color:#157ed2">KEI Warehouse</strong>
                    </div>
                </div>
              
            </div>
        </div>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="assets/plugins/jQuery/jQuery-2.1.3.min.js" crossorigin="anonymous"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/plugins/bootstrap/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="assets/plugins/paginationjs/dist/pagination.min.js" crossorigin="anonymous"></script>
    <script src="assets/plugins/toastr/toastr.min.js"></script>
    <script src="assets/plugins/sweetalert2/sweetalert2.min.js"></script>
    <script type="text/javascript" src="assets/js/toast.js"></script>	
  
    <script>
        let isLogin = localStorage.getItem("isLogin") ;
       // let cartCount = localStorage.getItem("cartCount");

        $(function () {
            var swiper = new Swiper(".mySwiper", {
                spaceBetween: 30,
                centeredSlides: true,
                autoplay: {
                    delay: 2500,
                    disableOnInteraction: false,
                },
                pagination: {
                    el: ".swiper-pagination",
                    clickable: true,
                },
                navigation: {
                    nextEl: ".swiper-button-next",
                    prevEl: ".swiper-button-prev",
                },
            });

           if(isLogin){
                if(isLogin == 1)
                {
                    $('[name="btnKeluar"]').show();
                    $('[name="btnLogin"]').hide();
                    $('.panel-login').show();
                    $('[name="edtSearch"]').attr('style', 'width:50vw');
                    $('.form-search').attr('style', 'margin-right: 0rem;');
                }
                else 
                {
                    $('[name="btnLogin"]').show();
                    $('[name="btnKeluar"]').hide();
                    $('[name="edtSearch"]').attr('style', 'width:60vw');
                    $('.form-search').attr('style', 'margin-right: 5rem;');
                    $('.panel-login').hide();
                }
           }else{
                localStorage.setItem("isLogin", 0);
           }
           
           getCartData();
            // if(cartCount){
            //     if(cartCount != 0){
            //         setValueCart(cartCount);
            //     }
            // }else{
            //     setValueCart(0);
            // }

            $('body').on('click', '[name="btnLogin"]', function(e){
                onClickLogin(e);
            });

            $('body').on('click', '#btnProsesLogin', function(e){
                onClickBtnProsesLogin(e);
            });

            $('body').on('click', '[name="btnKeluar"]', function(e){
                onClickLogout(e);
            });

            $('body').on('click', '[name="btnProfile"]', function(e){
                onClickProfile(e);
            });

            $('body').on('click', '[name="btnAddCart"]', function(e){
                onClickAddCart(e, this);
            });

            $('body').on('click', '#listPurchaseHistory', function(e){
                window.location.href= "./purchase_history.php"
            });

            // setting scrool
            btnScrollToTop = document.getElementById('btnScrollTop');
            btnScrollToTop.addEventListener("click", e => {
                window.scrollTo({
                    top: 0,
                    left: 0,
                    behavior: "smooth"
                });

                $('#btnScrollTop').css({ 
                    'opacity': 0.3,
                    'transition': 'opacity .3s ease-out'
                });
            });

            btnScrollToTop.addEventListener('mouseover', () => {
            // Change the button's background color
                $('#btnScrollTop').css({ 
                    'opacity': 1,
                    'transition': 'opacity .4s ease-out'
                });
            });

            btnScrollToTop.addEventListener('mouseout', () => {
            // Change the button's background color
                $('#btnScrollTop').css({ 
                    'opacity': 0.6,
                    'transition': 'opacity .4s ease-out'
                });
            });


            window.addEventListener('scroll', e => {
                // btnScrollToTop.style.display = window.scrollY > 20 ? 'block' : 'none';
                if(window.scrollY > 20){
                    $('#btnScrollTop').css({ 
                        'opacity': 0.6,
                        'transition': 'opacity .8s ease-out'
                        
                    });

                    setTimeout(() => {
                        // btnScrollToTop.style.display = 'none';
                        $('#btnScrollTop').css({ 
                            'opacity': 0.3,
                            'transition': 'opacity .8s ease-out'
                        });
                    }, 1800);
                }
               
            });

        });


        function onClickLogin(e) {
            // localStorage.setItem("isLogin", 1);
            // setTimeout(() => {
            //     window.location.reload();
            // }, 800);
            $('#modalLogin').modal('show');

        };

        function onClickProfile(e) {
            $('#modalProfile').modal('show');

        };

        function onClickBtnProsesLogin(e) {
            localStorage.setItem("isLogin", 1);
            setTimeout(() => {
                window.location.reload();
            }, 800);
        };

        function onClickLogout(e) {
            localStorage.setItem("isLogin", 0);
            setTimeout(() => {
                window.location.reload();
            }, 800);
        };

        function onClickAddCart(e, self){
            let idProd = $(self).attr('data-value');

            let oldCartCount = $('[name="cart-count"]').attr('data-value');
            let newCartCount = parseInt(oldCartCount) + 1;

            pushCartJson(idProd, 1);
            
            var toast = new Toast();
			toast.makeText(0, `Berhasil ditambahkan ke keranjang`, toast.LENGTH_SHORT, toast.POSITION_BOTTOM_RIGHT);
        }

        function setValueCart(newVal){
            $('[name="cart-count"]').attr('data-value', newVal);
            $('[name="cart-count"]').text(newVal);

            //console.log('onClickAddCart: '+ newVal);
        }

        // function getDetailProd(callBack, id){

        //     $.ajax({
        //         type: 'GET',
        //         url: 'data-products.json',
        //         dataType: "json",
        //         cache: false,
        //         success: function(response){
        //             let getData = response.filter(e=> e.id == id);
        //             callBack(getData);
        //         },
        //         error: function(xhr, testStat){
        //             console.error(xhr.responseText);
        //         }
        //     });
        // }

        function pushCartJson(id, qty){
            $.ajax({
                type: 'get',
                url: `http://localhost:1996/kei/api/pushcart.php`,
                data: {
                    id: id,
                    qty: qty
                },
                dataType: "json",
                cache: false,
                success: function(response){
                    getCartData();
                },
                error: function(xhr, testStat){
                    console.error(xhr.responseText);
                }
            });
        }

        function getCartData(){
            $.ajax({
                type: 'GET',
                url: 'data-cart-item.json',
                dataType: "json",
                cache: false,
                success: function(response){
                    //console.log(response);
                    let jmlData = response.length;
                    setValueCart(jmlData);
                },
                error: function(xhr, testStat){
                    console.error('error:', xhr.responseText);
                }
            });
        }
    </script>
</body>
</html>