<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>KEI Warehouse</title>
    <link href="./assets/img/favicon.ico" rel="icon">

    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css" crossorigin="anonymous">
    <link rel="stylesheet" href="assets/plugins/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/plugins/swiper/swiper-bundle.min.css"/>
    <link rel="stylesheet" href="assets/plugins/paginationjs/dist/pagination.css">
    <link rel="stylesheet" href="assets/plugins/toastr/toastr.css">
    <link rel="stylesheet" href="assets/plugins/pretty-checkbox/dist/pretty-checkbox.min.css">
    <link rel="stylesheet" href="assets/plugins/sweetalert2/sweetalert2.min.css">
    <link rel="stylesheet" href="assets/header.css">
    
    <script src="assets/plugins/swiper/swiper-bundle.min.js"></script>
    <style>
        @font-face {
            font-family: 'Barlow Regular';
            font-style: normal;
            font-weight: normal;
            src: local('Barlow Regular'), url('assets/fonts/barlow/Barlow-Regular.woff') format('woff');
        }
    
        body {
            font-family: 'Barlow Regular';
        }
    </style>

  </head>
  <body>
