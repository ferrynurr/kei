

<div class="hidden-xs">
    <nav class="navbar navbar-default navbar-fixed-top core-navbar ">
        <div class="panel-fer-topbar">
            <div class="panel-body panel-body-fer ">
                <div class="row">
                    <div class="col-md-6" style="padding-left: 3rem;">
                        <div class="flex-container pull-left visible-xs visible-sm visible-md" id="brand-sm">
                            <a class="text-header" href="./"><strong style="font-size: 16px;">KEI WAREHOUSE</strong></a>
                        </div>   
                    </div>
                    <div class="col-md-6">
                        <div class="flex-container hidden-xs pull-right">
                            <a href="#">About US</a>
                            <a href="#">Our Services</a>
                            <a href="#">Our Client </a>
                            <a href="#">Contact US </a>
                            <div class="visible-sm">
                                <button name="btnLogin" type="button" class="btn-fer btn-outline-light-fer"><span>Masuk</span></button>
                            </div>

                        </div>   
                    </div>

                </div>
            </div>
        </div>
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div id="brand-lg" class="navbar-header" >
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand text-header hidden-xs hidden-sm hidden-md" href="./"><strong>KEI WAREHOUSE</strong></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <!-- <div class="panel-pencarian"> -->
                <div style="float: right;">
                    <ul class="nav navbar-nav">
                        <li><a class="text-header" href="#"><i class="fa fa-caret-down text-header"></i> Kategori</a></li>
                    </ul>
                    <form class="navbar-form navbar-left form-search">
                        <div class="input-group">
                            <input name="edtSearch" type="text" class="form-control btn-rounded" style="width: 50vw;" placeholder="Cari di KEI Warehouse...">
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-rounded" type="button"> <i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                <!-- </div> -->
            
            <!-- <div style="float: right; background-color:red;"> -->
                    <ul class="nav navbar-nav panel-login">
                        <li>
                            <a href="./cart.php">
                                <i class="fa fa-shopping-cart text-header" style="font-size: 22px;"></i>
                                <span class="badge badge-fer" data-value="0" name="cart-count"></span>
                            </a>
                        </li>
                        <li>
                            <a href="#" name="btnProfile" class="btnProfile"><i class="fa fa-user-circle text-header" style="font-size: 22px;"></i></a>
                        </li>
                    </ul>
            
                    <form class="navbar-form navbar-right right-panel-header border-vertical hidden-sm"> 
                        <button name="btnLogin" type="button" class="btn-fer btn-outline-light-fer"><span>Masuk</span></button>
                        <!-- <button name="btnKeluar" type="button" class="btn-fer btn-light-fer"><span>Keluar</span></button> -->
                        <!-- <button type="button" class="btn-fer btn-primary-fer">Daftar</button> -->
                       
                

                    </form>   
                </div>

            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>

<div class="visible-xs">
    <nav class="navbar navbar-default navbar-fixed-top core-navbar" style="padding: 1rem;">
        <div class="text-left"> <a class="text-header" href="./"><strong style="font-size: 10px;">KEI WAREHOUSE</strong></a></div>
       
        <div class="row" style="margin-top:10px;">
            <div class="col-xs-1" style="padding-top:3px;">
                <a class="text-header" href="#"><i class="fa fa-th-large text-header" style="font-size: 22px;"></i></a>
            </div>
            <div class="col-xs-8">
                <div class="input-group">
                    <input type="text" class="form-control input-sm" placeholder="Cari di KEI Warehouse...">
                    <span class="input-group-btn">
                            <button class="btn btn-default btn-sm" type="button"> <i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="pull-right panel-login" style="gap:3rem; display:flex;padding-top:3px;">
                    <a href="./cart.php">
                        <i class="fa fa-shopping-cart text-header" style="font-size: 22px;"></i>
                        <span class="badge badge-fer-sm" data-value="0" name="cart-count"></span>
                    </a>
                    <a href="#" name="btnProfile" class="btnProfile"><i class="fa fa-user-circle text-header" style="font-size: 22px;"></i></a>
                </div>

                <button name="btnLogin" type="button" class="btn-fer btn-outline-light-fer"><span>Masuk</span></button>
            </div>
        </div>
           
    
            
      
    </nav>
</div>