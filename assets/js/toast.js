//pastikan referensi link toaster berada sebelum link ref toast.js
function Toast(){
	this.LENGTH_SHORT = '2000';
	this.LENGTH_LONG = '6000';
	
	this.POSITION_TOP_LEFT = 'toast-top-right';
	this.POSITION_TOP = 'toast-top-center';
	this.POSITION_TOP_RIGHT = 'toast-top-right';
	this.POSITION_BOTTOM_LEFT = 'toast-bottom-right';
	this.POSITION_BOTTOM = 'toast-bottom-center';
	this.POSITION_BOTTOM_RIGHT = 'toast-bottom-right';
	
	this.TYPE_SUCCESS = 0;
	this.TYPE_INFO = 1;
	this.TYPE_WARNING = 2;
	this.TYPE_ERROR = 3;
}

Toast.prototype.makeText = function(type, message, duration, position){
    toastr.options = {
	  "closeButton": false,
	  "debug": false,
	  "newestOnTop": true,
	  "progressBar": false,
	  "positionClass": position,
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": duration,
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	}
	switch(type){
		case this.TYPE_SUCCESS :{			
			toastr.success(message);		
			break;
		}
		case this.TYPE_INFO :{			
			toastr.info(message);
			break;		
		}
		case this.TYPE_WARNING :{			
			toastr.warning(message);
			break;		
		}
		default : {			
			toastr.error(message);
			break;		
		}
	}
};