
<?php include 'templates/header.php';?>
<?php include 'templates/topbar.php';?>

<div class="content-home">
    <div class="container">
        <!-- Swiper -->
        <div class="swiper mySwiper" style="display: none;">
            <div class="swiper-wrapper">
                <div class="swiper-slide">
                    <img src="assets/banner/b1.jpg" alt="">
                </div>
                <div class="swiper-slide">
                    <img src="assets/banner/b2.jpg" alt="">
                </div>
                <div class="swiper-slide">
                    <img src="assets/banner/b3.jpg" alt="">
                </div>
                <div class="swiper-slide">
                    <img src="assets/banner/b4.jpg" alt="">
                </div>
            </div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-pagination"></div>
        </div>

        <div id="productList"></div>
        <div id="pagination-container" style="padding-bottom: 3rem;"></div>
    </div>

</div>

<?php include 'templates/footer.php';?>

<script>
    $(document).ready(function () {
        getProduct();
        
       
    });

    function getProduct(){

        $('#pagination-container').pagination({
            pageSize: 8,
            dataSource: function(done){
                $.ajax({
                    type: 'GET',
                    url: 'data-products.json',
                    dataType: "json",
                    cache: false,
                    beforeSend: function() {
                        // setting a timeout
                        $('#productList').html(loading());
                    },
                    success: function(response){
                        done(response);
                    },
                    error: function(xhr, testStat){
                        console.error(xhr.responseText);
                        $('#productList').html('');
                    }
                });
            },
            callback: function(done, pagination) {
                // template method of yourself
                var html = createCard(done);
                $('#productList').html(html);
            }
        });
    }

    function createCard(dataProducts) {

        let html = `<div class="row">`;
        $.each(dataProducts, function (i, val) { 

            var length = 50;
            var trimmedString = val.title.length > length ? 
                val.title.substring(0, length - 3) + "..." : 
                val.title;

            let harga ='<strong>Rp. xxx.xxxx</strong>';

            if(isLogin == 1){
                harga = val.price == val.price_disc 
                        ? ` <strong>Rp ${val.price_disc}</strong>` 
                        : ` <s style="color:#938f8f; font-size:12px;">Rp ${val.price}</s>
                                    <strong>Rp ${val.price_disc}</strong>` ;
            }

            let rateView =``;
            let oldRate = parseInt(val.rate);
            for(let i=1; i <= 5; i++){
                if(oldRate > 0)
                    rateView += `<span class="fa fa-star checked-rate"></span>`;
                else 
                    rateView += `<span class="fa fa-star" style="color:#c3c3c3"></span>`;

                oldRate--;
            }   

            html += `
                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="thumbnail panel-item-product btn-rounded">
                            <img src="${val.photo}" alt="..." style="height: 200px;">
                            <div class="caption">
                                <div style="height:55px">
                                    <strong style="font-size:13px;">${trimmedString}</strong>
                                </div>
                                
                                <h4>
                                   ${harga}
                                </h4>
                                <div class="row">
                                    <div class="col-md-7 col-xs-7">
                                        ${rateView} <small>${val.rate}</small>
                                    </div>
                                    <div class="col-md-5 col-xs-5 text-right" >
                                        <small>${val.sold} Terjual</small>
                                    </div>
                                </div>
                            </div>
                            ${ isLogin == 1 ? 
                                `<div style="padding-top:1rem;">
                                    <button name="btnAddCart" data-value="${val.id}" type="button" class="btn btn-primary btn-sm btn-rounded addCart" style="width:100%"><i class="fa fa-shopping-cart"></i> Keranjang</button>
                                </div>
                                `
                                : ``}
                            
                        </div>
                    </div>
                
             `;
        });

        html += '</div>';

        return html;
    }

    function loading() {
        let html =`
            <div class="text-center" style="margin-top:20vh;">
                <i class="fa fa-spinner fa-spin fa-2x"></i>&nbsp;&nbsp;
                <span style="font-size:18px"> Sedang memuat data...</span>
            </div>
        `;
        return html;

    }
</script>
